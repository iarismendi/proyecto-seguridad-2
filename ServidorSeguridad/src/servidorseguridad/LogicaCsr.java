/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorseguridad;

import dominio.Csr;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import sun.security.pkcs.PKCS10;
import sun.security.x509.X500Name;
import sun.security.x509.X500Signer;

/**
 *
 * @author Usuario1
 */
public class LogicaCsr {    
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private KeyPairGenerator keyGen;
    private X500Name x500Name;
    
    public LogicaCsr(){
        try {
            this.keyGen = KeyPairGenerator.getInstance("RSA");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        this.keyGen.initialize(2048, new SecureRandom());
        KeyPair keypair = this.keyGen.generateKeyPair();
        this.publicKey = keypair.getPublic();
        this.privateKey = keypair.getPrivate();
    }
    
    public PublicKey getClavePublica() {
        return this.publicKey;
    }
 
    public PrivateKey getClavePrivada() {
        return this.privateKey;
    }
    
    public X500Name getX500Name(){
        return this.x500Name;
    }
    
    public String generarCsr(Csr csr) throws Exception {
        // generate PKCS10 certificate request
        String sigAlg = "MD5WithRSA";
        PKCS10 pkcs10 = new PKCS10(this.publicKey);
        Signature signature = Signature.getInstance(sigAlg);
        signature.initSign(privateKey);
        // common, orgUnit, org, locality, state, country
        this.x500Name = new X500Name(csr.getCommonName(),csr.getOrganizationUnit(),
                                csr.getOrganizationName(), csr.getLocalityName(), 
                                csr.getStateName(), csr.getCountry());
        pkcs10.encodeAndSign(new X500Signer(signature, x500Name));
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bs);
        pkcs10.print(ps);
        byte[] c = bs.toByteArray();
        try {
            if (ps != null)
                ps.close();
            if (bs != null)
                bs.close();
        } catch (Throwable th) {
        }
        return new String(c);
    }
    
}
