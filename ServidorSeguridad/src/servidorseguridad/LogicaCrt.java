/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorseguridad;

import biz.source_code.base64Coder.Base64Coder;
import dominio.Csr;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.Attribute;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import sun.security.pkcs.PKCS10;

/**
 *
 * @author Isaac Arismendi
 */
public class LogicaCrt {
    
    public  static PrivateKey  obtenerKey() throws Exception{
        FileInputStream archivoKey = new FileInputStream("src/certificados/KeyKI.jks");
        char[] KeyStoreClave = "KI12345".toCharArray();
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(archivoKey, KeyStoreClave);
        KeyStore.PrivateKeyEntry pE = (KeyStore.PrivateKeyEntry) keyStore.getEntry("ki", new KeyStore.PasswordProtection(KeyStoreClave));
        PrivateKey LlavePrivada = pE.getPrivateKey();
        return LlavePrivada; 
    }
    
    public static X509Certificate  obtenerCrt() throws Exception{
        FileInputStream archivoKey = new FileInputStream("src/certificados/KeyKI.jks");
        char[] KeyStoreClave = "KI12345".toCharArray();
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(archivoKey, KeyStoreClave);
        KeyStore.PrivateKeyEntry pE = (KeyStore.PrivateKeyEntry) keyStore.getEntry("ki", new KeyStore.PasswordProtection(KeyStoreClave));
        X509Certificate crt = (X509Certificate) pE.getCertificate();
        return crt; 
    }
    
    public static String firmar(Csr csr) throws Exception{
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        KeyPair rootPair = new KeyPair(obtenerCrt().getPublicKey() , obtenerKey());
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        
        LogicaCsr logicaCsr = new LogicaCsr();
        logicaCsr.generarCsr(csr);
        
        PublicKey clavePublica = logicaCsr.getClavePublica();
        PrivateKey clavePrivada = logicaCsr.getClavePrivada();
        PKCS10CertificationRequest request = new PKCS10CertificationRequest("MD5withRSA",
                logicaCsr.getX500Name().asX500Principal(), clavePublica, null, clavePrivada ) ;
        Calendar c=Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, 5);
        certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
        certGen.setIssuerDN(obtenerCrt().getSubjectX500Principal());
        certGen.setNotBefore(new Date(System.currentTimeMillis()));
        certGen.setNotAfter(c.getTime());
        X500Principal x500Principal=new X500Principal(request.getCertificationRequestInfo().getSubject().toString());
        System.out.println(logicaCsr.getX500Name().toString());
        certGen.setSubjectDN(x500Principal);
        certGen.setPublicKey(clavePublica);
        certGen.setSignatureAlgorithm("SHA256WithRSAEncryption");
        certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
            new AuthorityKeyIdentifierStructure(obtenerCrt()));

        certGen.addExtension(X509Extensions.SubjectKeyIdentifier,

        false, new SubjectKeyIdentifierStructure(clavePublica));

        certGen.addExtension(X509Extensions.BasicConstraints, true, new BasicConstraints(false));

        certGen.addExtension(X509Extensions.KeyUsage, true, new KeyUsage(KeyUsage.digitalSignature
            | KeyUsage.keyEncipherment));

        certGen.addExtension(X509Extensions.ExtendedKeyUsage, true, new ExtendedKeyUsage(
            KeyPurposeId.id_kp_serverAuth));

        ASN1Set attributes = request.getCertificationRequestInfo().getAttributes();
        if(attributes!=null)
            for (int i = 0; i != attributes.size(); i++) {
                Attribute attr = Attribute.getInstance(attributes.getObjectAt(i));

                if (attr.getAttrType().equals(PKCSObjectIdentifiers.pkcs_9_at_extensionRequest)) {
                    X509Extensions extensions = X509Extensions.getInstance(attr.getAttrValues().getObjectAt(0));

                    Enumeration en = extensions.oids();
                  while (en.hasMoreElements()) {
                    DERObjectIdentifier oid = (DERObjectIdentifier) en.nextElement();
                    X509Extension ext = extensions.getExtension(oid);

                    certGen.addExtension(oid, ext.isCritical(), ext.getValue().getOctets());
            }
          }
        }
        X509Certificate usuarioCrt = certGen.generateX509Certificate(rootPair.getPrivate());
        usuarioCrt.getEncoded();
        String resultado = imprimirCrt(usuarioCrt.getEncoded());
        return  resultado;
    }
    
    
     public static String imprimirCrt(byte[] encoded) throws Exception{
       
       String stringCrt = "";
       char[] b64 = Base64Coder.encode(encoded);
        stringCrt = stringCrt + "-----BEGIN CERTIFICATE-----\n";
        for (String subSeq : splitCrt(b64, 64)) {
            stringCrt = stringCrt + subSeq + "\n";
        }
       stringCrt = stringCrt + "-----END CERTIFICATE-----";
       return  stringCrt;
   }

  private static Vector<String> splitCrt(char[] chry, int subarrLen) {
        Vector<String> result = new Vector<String>();
        String input = new String(chry);
        int i = 0;
        while (i < chry.length) {
            result.add(input.substring(i, Math.min(input.length(), i + subarrLen)));
            i = i + subarrLen;
        }
        return result;
    }
    
}
