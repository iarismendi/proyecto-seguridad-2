/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorseguridad;

import conexion.Conexion;
import dominio.Csr;
import dominio.Usuario;
import dominio.UsuarioXML;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
//import sun.security.*;

/**
 *
 * @author Isaac Arismendi y Karem Segura
 */
public class GestionServidor implements Runnable{

    private SSLSocket cliente;
    private Thread hilo;
        
    public GestionServidor(SSLSocket cliente){
        this.cliente = cliente;
        hilo = new Thread(this);
        hilo.start();
    }
    
     private Object recibirObjeto() throws IOException{
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(this.cliente.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException ex ){
          
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }
     
     private void enviarObjeto(Object mensaje) throws IOException{
        ObjectOutputStream enviarMensaje = new ObjectOutputStream(this.cliente.getOutputStream());
        enviarMensaje.writeObject(mensaje);
    }
    
    public String generarCrs(Csr csr ) throws IOException, CertificateException, Exception{
//         LogicaCsr generarCsr = new LogicaCsr();
//         String stringCsr = generarCsr.generarCsr(csr);

         return LogicaCrt.firmar(csr);
    }
    
    @Override
    public void run() {
        Csr csr;
        try {
           String mensajeCliente = (String) recibirObjeto();
           if(mensajeCliente.equals("csr")){
                csr = (Csr) recibirObjeto();
                String stringCsr = generarCrs(csr);
                enviarObjeto(stringCsr);
                enviarObjeto(LogicaCrt.firmar(csr));
            }
           if(mensajeCliente.equals("agregar cliente")){
               Usuario usuario = (Usuario) recibirObjeto();
               Boolean bool = UsuarioXML.agregarConValidacion(usuario);
               enviarObjeto(bool);
           }          
           if(mensajeCliente.equals("inactivar cliente")){
               Usuario usuario = (Usuario) recibirObjeto();
               UsuarioXML.inactivarUsuario(usuario);
               enviarObjeto(Boolean.TRUE);
           }
           if(mensajeCliente.equals("login")){
               Usuario usuario = (Usuario) recibirObjeto();
               int error = UsuarioXML.usuarioValido(usuario);
               enviarObjeto(error);
           } 
        } catch (IOException ex) {
            Logger.getLogger(GestionServidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GestionServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        
    }
    
}
