/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import servidorseguridad.GestionServidor;


/**
 *
 * @author Isaac Arismendi y Karem Segura
 */
public class Conexion implements Runnable{
    private final int PUERTO = 7777;
    private SSLServerSocket servidor;
    private Thread hilo;
    
    public Conexion() throws IOException{
        System.setProperty("javax.net.ssl.keyStore","src/certificados/KeySocketKI.jks");
        System.setProperty("javax.net.ssl.keyStorePassword","KI12345");
        ServerSocketFactory ssl = SSLServerSocketFactory.getDefault();
        this.servidor = (SSLServerSocket) ssl.createServerSocket(PUERTO);
        System.out.println("Servidor Encendido");
        hilo = new Thread(this);
        hilo.start();
    }

    @Override
    public void run() {
    
        while(true)
        {
            try {
                SSLSocket cliente = (SSLSocket) servidor.accept();
                new GestionServidor(cliente);
            }
            catch (IOException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
