/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
/**
 *
 * @author karemsegura
 */
public class UsuarioXML {
    private static String PATH = "src/xml/usuarios.xml";
    private static String USUARIOS = "usuarios";
    private static String USUARIO = "usuario";
    private static String NOMBRE = "nombre";
    private static String APELLIDO = "apellido";
    private static String CORREO = "correo";
    private static String NOMBRE_USUARIO = "nombre_usuario";
    private static String CLAVE = "clave";
    private static String STATUS = "status";

    public static int usuarioValido(Usuario usuario) throws JDOMException, IOException{
        
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();
        
        try {
            doc = builder.build(PATH);
            root = doc.getRootElement();
            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);                
                //verifico login correcto
                if (child.getAttributeValue(NOMBRE_USUARIO).equals(usuario.getUsuario()) 
                 && (child.getAttribute(CLAVE).getValue()).equals(usuario.getClave()) ) {
                    if ( (child.getAttribute(STATUS).getValue()).equals("activo")) 
                      return 1; // el usuario existe y esta activo
                    else
                      return 2; // el usuario existe pero esta bloqueado
               }
               pos++;
            }
        } catch (JDOMParseException e) {
            System.out.println(e.toString());
        }
        
        return 0; // no existe el usuario con esa clave
    }
    
    public static void inactivarUsuario(Usuario usuario) throws JDOMException, IOException{
        Document doc;
        Element root, child;
        Iterator it;
        boolean found = false;
        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(PATH);
            root = doc.getRootElement();
            it = root.getChildren(USUARIO).iterator();
            while (!found && it.hasNext()) {
                child = (Element) it.next();
                if ( child.getAttributeValue(NOMBRE_USUARIO).equals(usuario.getUsuario()) ) {
                        child.setAttribute(STATUS, "inactivo");
                        found = true;
                }
            }
            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);
                try {
                    FileOutputStream file = new FileOutputStream(PATH);
                    out.output(doc, file);
                    file.flush();
                    file.close();
                } catch (Exception e){
                    
                }               
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        } catch (JDOMParseException e) {
            System.out.println(e.toString());
        }
    }

    public static boolean agregarUsuario(Usuario usuario) throws IOException{
        
        Document doc;
        Element root, newChild;
        SAXBuilder builder = new SAXBuilder();

        try {
            doc = builder.build(PATH);
            root = doc.getRootElement();
            newChild = new Element(USUARIO);

            newChild.setAttribute(NOMBRE, usuario.getNombre());
            newChild.setAttribute(APELLIDO, usuario.getApellido());
            newChild.setAttribute(CORREO, usuario.getCorreo());
            newChild.setAttribute(NOMBRE_USUARIO, usuario.getUsuario());
            newChild.setAttribute(CLAVE, usuario.getClave());
            newChild.setAttribute(STATUS, "activo");

            root.addContent(newChild);

            try {
                Format format = Format.getPrettyFormat();
                XMLOutputter out = new XMLOutputter(format);

                try {
                    FileOutputStream file = new FileOutputStream(PATH);
                    out.output(doc, file);
                    file.flush();

                } catch(Exception e) {
                    
                }
                return true;
            } catch (Exception ex) {

                System.out.println(ex.toString());

            }
        } catch (JDOMException ex) {
            System.out.println(ex.toString());
        }
        return false;
    }

    public static boolean existeUsuario(Usuario usuario) throws JDOMException, IOException{
         
        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();
        
        try {
            doc = builder.build(PATH);
            root = doc.getRootElement();
            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);
                
                //verifico si el nombre de usuario ya existe
                if ( child.getAttributeValue(NOMBRE_USUARIO).equals(usuario.getUsuario()) ){
                    return true;
               }
               pos++;
            }

        } catch (JDOMParseException e) {
            System.out.println(e.toString());
        }
        
        return false;
    }

    public static boolean agregarConValidacion(Usuario usuario) throws JDOMException, IOException{
        if (!existeUsuario(usuario))
            return agregarUsuario(usuario);
        return false;
    }
}
