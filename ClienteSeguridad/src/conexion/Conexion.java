/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


/**
 *
 * @author Usuario1
 */
public class Conexion {
    private String ipServidor;
    private int numeroPuerto = 7777;
    private SSLSocket servidor;
    
    public Conexion() throws IOException{
       this.ipServidor = "localhost";
    }
    
    public Conexion(String ip) throws IOException{
       this.ipServidor = ip;
    }
    
    public void conectar() throws IOException{
        System.setProperty("javax.net.ssl.trustStore","src/certificados/KeySocketKI.jks");
        System.setProperty("javax.net.ssl.keyStorePassword","KI12345");
        SSLSocketFactory ssl = (SSLSocketFactory) SSLSocketFactory.getDefault();
        this.servidor = (SSLSocket) ssl.createSocket(this.ipServidor, this.numeroPuerto);
        String[] suites = this.servidor.getSupportedCipherSuites();
        this.servidor.setEnabledCipherSuites(suites);
        this.servidor.startHandshake(); 
    }
    
    public void enviarObjeto(Object mensaje) throws IOException{
       ObjectOutputStream enviarMensaje = new ObjectOutputStream(servidor.getOutputStream());
       enviarMensaje.writeObject(mensaje);
   }
    
    public Object recibirObjeto() throws IOException{
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(servidor.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException ex) {
          
        }   
        return null;
    }
    
}
