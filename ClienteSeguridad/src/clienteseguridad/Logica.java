/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteseguridad;

import conexion.Conexion;
import dominio.Csr;
import dominio.Usuario;
import java.io.IOException;

/**
 *
 * @author Isaac Arismendi
 */
public class Logica {
    
    private static String stringCrt;
    
    public static String generarCsrYCrt(Csr csr) throws IOException{
        Conexion servidor = new Conexion();
        servidor.conectar();
        servidor.enviarObjeto("csr");
        servidor.enviarObjeto(csr);
        String stringCsr = (String) servidor.recibirObjeto();
        stringCrt = (String) servidor.recibirObjeto();
        return stringCsr;
    }
    
    public static String obtenerCrt(){
        return stringCrt;
    }
    
    public static Boolean agregarUsuario(Usuario usuario) throws IOException{
        Conexion servidor = new Conexion();
        servidor.conectar();
        servidor.enviarObjeto("agregar cliente");
        servidor.enviarObjeto(usuario);
        return (Boolean) servidor.recibirObjeto();
        
    }
    
    public static Boolean inactivarUsuario(Usuario usuario) throws IOException{
        Conexion servidor = new Conexion();
        servidor.conectar();
        servidor.enviarObjeto("inactivar cliente");
        servidor.enviarObjeto(usuario);
        return (Boolean) servidor.recibirObjeto();
        
    }
     
    public static int login(Usuario usuario) throws IOException{
        Conexion servidor = new Conexion();
        servidor.conectar();
        servidor.enviarObjeto("login");
        servidor.enviarObjeto(usuario);
        return (Integer) servidor.recibirObjeto();
    }
    
}
