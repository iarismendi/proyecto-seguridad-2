/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.List;
import java.io.IOException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author karemsegura
 */
public class CaptchaXML {
    private static String PATH = "src/xml/captchas.xml";
    private static String CAPTCHAS = "captchas";
    private static String CAPTCHA = "captcha";
    private static String ID = "id";
    private static String RUTA = "ruta";
    private static String VALOR = "valor";

    public static Captcha buscarCaptcha(int numeroRandom) throws JDOMException, IOException {

        Document doc;
        Element root, child;
        List<Element> rootChildrens;
        boolean found = false;
        int pos = 0;
        SAXBuilder builder = new SAXBuilder();

        Captcha captcha = new Captcha();

        try {          
            doc = builder.build(PATH);
            root = doc.getRootElement();
            rootChildrens = root.getChildren();

            while (!found && pos < rootChildrens.size()) {
                child = rootChildrens.get(pos);
                
                if (child.getAttributeValue(ID).equals(String.valueOf(numeroRandom))) {   
                    captcha.setId(numeroRandom);
                    captcha.setRuta(child.getAttributeValue(RUTA));
                    captcha.setRuta(child.getAttributeValue(VALOR));
                    found = true;
                }
                pos++;
            }
        } catch (JDOMParseException e) {
            System.out.println(e.toString());
        }
        return captcha;

    }
}
